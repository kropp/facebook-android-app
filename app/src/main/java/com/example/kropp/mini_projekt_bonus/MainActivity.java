package com.example.kropp.mini_projekt_bonus;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.LikeView;
import com.facebook.share.widget.ShareButton;

public class MainActivity extends Activity {

    CallbackManager cm;
    AccessToken at;
    Profile p;
    ProfileTracker pt;
    TextView tv;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_main);

        ShareLinkContent content = new ShareLinkContent.Builder()
                .setImageUrl(Uri.parse("https://developers.facebook.com"))
                .build();

        tv = (TextView) findViewById(R.id.mainText);
        cm = CallbackManager.Factory.create();
        LoginButton lb = (LoginButton) findViewById(R.id.login_button);
        ShareButton shareButton = (ShareButton) findViewById(R.id.share_button);

        shareButton.setShareContent(content);

        LikeView likeView = (LikeView) findViewById(R.id.likeView);
        likeView.setLikeViewStyle(LikeView.Style.STANDARD);
        likeView.setAuxiliaryViewPosition(LikeView.AuxiliaryViewPosition.INLINE);

        likeView.setObjectIdAndType(
                "http://inthecheesefactory.com/blog/understand-android-activity-launchmode/en",
                LikeView.ObjectType.OPEN_GRAPH);

        lb.registerCallback(cm, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                at = loginResult.getAccessToken();
                tv.setText("Witaj " + Profile.getCurrentProfile().getFirstName());
                pt = new ProfileTracker() {
                    @Override
                    protected void onCurrentProfileChanged(Profile oldProfile, Profile currentProfile) {
                        p = Profile.getCurrentProfile();
                        tv.setText("Welcome " + p.getFirstName());
                    }
                };
                pt.stopTracking();
            }

            @Override
            public void onCancel() {
                Toast.makeText(getApplicationContext(), "Canceled." , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getApplicationContext(), "Error!" , Toast.LENGTH_SHORT).show();

            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        cm.onActivityResult(requestCode, resultCode, data);
    }



}
